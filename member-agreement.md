---
title: Snowdrift.coop Cooperative Membership Agreement
categories: legal
...

**This page is currently a stub but will eventually become the membership
agreement for the Snowdrift.coop cooperative, which candidates will need to sign
before becoming members in good standing.**

* Much of this will likely be similar to the [Terms of Use](terms-of-use):
  prohibitions on disruptive or abusive behavior, restriction of liability.

* However there will also be co-op specific issues like explaining the member
  class system and the circumstances under which class will be reassigned. Much
  of this will flow directly from the Bylaws and the Standing Rules.
    * The Terms of Use bind users to future versions, although they do require
      public comment periods before substantive revisions take effect. Do we
      want the Membership Agreement to do likewise or to require members to
      re-sign it when changes take effect?
    * Member-owners' role in the organization and how not to misrepresent it
      when interacting with non-members.

* This will also need to include a system to issue the statutorily required
  membership certificate, which lists certain information about the member's
  rights within the cooperative.
