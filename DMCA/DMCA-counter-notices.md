# Guide to Submitting a DMCA Counter Notice

*Version 0.1*

This guide describes the information that Snowdrift.coop needs in order to process a counter notice to a DMCA takedown request. If you have more general questions about what the DMCA is or how we process DMCA takedown requests, please review our [DMCA Takedown Policy](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-takedowns.md).

If you believe your content on was mistakenly disabled by a DMCA takedown request, you have the right to contest the takedown by submitting a counter notice. If you do, we will wait 10-14 days and then re-enable your content unless the copyright owner initiates a legal action against you before then. Our counter-notice form, set forth below, is consistent with the form suggested by the DMCA statute (which can be found at the [U.S. Copyright Office website](https://www.copyright.gov).

As with all legal matters, it is always best to consult with a professional about your specific questions or situation. We strongly encourage you to do so before taking any action that might impact your rights. This guide isn't legal advice and shouldn't be taken as such.

### Before You Start

***Tell the Truth.***
The DMCA requires that you swear to your counter notice *under penalty of perjury*. It is a federal crime to intentionally lie in a sworn declaration. (*See* [U.S. Code, Title 18, Section 1621](https://www.gpo.gov/fdsys/pkg/USCODE-2011-title18/html/USCODE-2011-title18-partI-chap79-sec1621.htm).) Submitting false information could also result in civil liability—that is, you could get sued for money damages.

***Investigate.***
Submitting a DMCA counter notice can have real legal consequences. If the complaining party disagrees that their takedown notice was mistaken, they might decide to file a lawsuit against you to keep the content disabled. You should conduct a thorough investigation into the allegations made in the takedown notice and probably talk to a lawyer before submitting a counter notice.

***You Must Have a Good Reason to Submit a Counter Notice.***
In order to file a counter notice, you must have "a good faith belief that the material was removed or disabled as a result of mistake or misidentification of the material to be removed or disabled." ([U.S. Code, Title 17, Section 512(g)](https://www.copyright.gov/title17/92chap5.html#512).) Whether you decide to explain why you believe there was a mistake is up to you and your lawyer, but you *do* need to identify a mistake before you submit a counter notice. Counter notices may cite mistakes in the takedown notice such as: the complaining party doesn't have the copyright; the work is being used legally under an applicable license; or the complaint doesn't account for the fact that my use is protected by the fair-use doctrine; or other defects with the takedown notice.

***Copyright Laws Are Complicated.***
Sometimes a takedown notice might allege infringement in a way that seems odd or indirect. Copyright laws are complicated and can lead to some unexpected results. For example, a copyright complaint might also be based on [non-literal copying](https://en.wikipedia.org/wiki/Substantial_similarity) such as a claim based in saying they think a *design* or text is too similar to a copyrighted one.

This is just one example of the complexities of copyright law. Since there are many nuances to the law and some unsettled questions in these types of cases, it is especially important to get professional advice if the infringement allegations do not seem straightforward.

***A Counter Notice Is A Legal Statement.***
We require you to fill out all fields of a counter notice completely, because a counter notice is a legal statement — not just to us, but to the complaining party. As we mentioned above, if the complaining party wishes to keep the content disabled after receiving a counter notice, they will need to initiate a legal action seeking a court order to restrain you from engaging in infringing activity relating to the content on Snowdrift.coop. In other words, you might get sued (and you consent to that in the counter notice).

***Your Counter Notice Will Be Published.***
As noted in our [DMCA Guide](https://gitlab.com/snowdrift/legal/blob/master/DMCA/DMCA-guide.md#d-transparency), **after redacting personal information,** we publish all complete and actionable counter notices at our [public repository](https://gitlab.com/snowdrift/legal/tree/master/DMCA/notice-archives/). Please also note that, although we will only publicly publish redacted notices, we may provide a complete unredacted copy of any notices we receive directly to any party whose rights would be affected by it. If you are concerned about your privacy, you may have a lawyer or other legal representative file the counter notice on your behalf.

***Snowdrift.coop Isn't The Judge.***
Snowdrift.coop exercises little discretion in this process other than determining whether the notices meet the minimum requirements of the DMCA. It is up to the parties (and their lawyers) to evaluate the merit of their claims, bearing in mind that notices must be made under penalty of perjury.

***Additional Resources.***
If you need additional help, there are many self-help resources online. Lumen has an informative set of guides on [copyright](https://www.lumendatabase.org/topics/5) and [DMCA safe harbor](https://www.lumendatabase.org/topics/14). If you think you have a particularly challenging case, the [Electronic Frontier Foundation](https://www.eff.org/pages/legal-assistance) may also be willing to help directly or refer you to a lawyer. If you are involved with a free/libre/open software project in need of legal advice, consider contacting the [Software Freedom Law Center](https://www.softwarefreedom.org/about/contact/) or [Software Freedom Conservancy](https://sfconservancy.org/).

### Your Counter Notice Must...

1. **Include the following statement: "I have read and understand Snowdrift.coop's Guide to Filing a DMCA Counter Notice."**
We won't refuse to process an otherwise complete counter notice if you don't include this statement; however, we will know that you haven't read these guidelines and may ask you to go back and do so.

2. ***Identify the content that was disabled and the location where it appeared.***
The disabled content should have been identified by URL in the takedown notice. You simply need to copy the URL(s) that you want to challenge.

3. **Provide your contact information.**
Include your email address, name, telephone number, and physical address.

4. ***Include the following statement: "I swear, under penalty of perjury, that I have a good-faith belief that the material was removed or disabled as a result of a mistake or misidentification of the material to be removed or disabled."***
You may also choose to communicate the reasons why you believe there was a mistake or misidentification. If you think of your counter notice as a "note" to the complaining party, this is a chance to explain why they should not take the next step and file a lawsuit in response. This is yet another reason to work with a lawyer when submitting a counter notice.

5. ***Include the following statement: "I consent to the jurisdiction of Federal District Court for the judicial district in which my address is located (if in the United States, otherwise in the judicial district where Snowdrift.coop is located), and I will accept service of process from the person who provided the DMCA notification or an agent of such person."***

6. **Include your physical or electronic signature.** Signatures may be a physical signature or a digital signature in a recognized industry-standard format such as PGP. Unsigned notifications will not be processed.

### How to Submit Your Counter Notice

You can send an email notification to <copyright@snowdrift.coop>. You may include an attachment if you like, but please also include a plain-text version of your letter in the body of your message.

If you must send your notice by physical mail, you can do that too, but it will take *substantially* longer for us to receive and respond to it—and the 10-14 day waiting period starts from when we *receive* your counter notice. Notices we receive via plain-text email have a much faster turnaround than PDF attachments or physical mail. If you still wish to mail us your notice, our physical mailing address is:

```
Snowdrift.coop
1021 Nottingham Rd.
Grosse Pointe Park, MI 48230
```
