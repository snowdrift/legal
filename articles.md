---
title: Articles of Incorporation
categories: legal
...

Below are our Articles as filed with the State of Michigan, originally 2013-08-12, restated (major changes) 2021-12-15. Some personal details are ommitted here.

## ARTICLES OF INCORPORATION

**Pursuant to the provisions of Act 162, Public Acts of 1982, the undersigned corporation executes the following Articles:**

### ARTICLE I

The name of the corporation is Snowdrift.coop, Inc.

### ARTICLE II

This cooperative corporation is formed under the Michigan Consumer Cooperative Act, Sec. 450.3100 et seq., on a cooperative basis for the benefit of its members to own and operate an online platform and to undertake any other lawful activity that supports the development and maintenance of Public Goods. “Public Goods” are non-rivalrous works which any member of the general public may freely and practically access, copy, use, adapt, and redistribute. In furtherance of its Public Goods mission, the corporation is authorized to pay reasonable compensation for services actually rendered and to make payments and distributions, including the distribution of funds to members in support of their Public Goods Projects.

### ARTICLE III

The corporation is organized upon a nonstock basis.

The corporation is to be financed under the following general plan:

The corporation is to be financed by membership fees pursuant to MCLA 450.3134 as well as potential loans or grants and sale of trademarked merchandise. All income, whether from fees or otherwise, shall be used to pay for goods and services to further and maintain the nonprofit services of the entity and the production of Public Goods.

The corporation is organized upon a membership basis.

### ARTICLE IV

The street address of the registered office of the corporation and the name of the resident agent at the registered office:

Andrew Smith, --- -----, ----, MI 481--

### ARTICLE V

The names and address of the incorporator is as follows:

Aaron Wolf              --- --- --- Ann Arbor, MI 4810- 

### ARTICLE VI

The management of the corporation is vested in a Board of Directors elected in accordance with the corporation's Bylaws.

### ARTICLE VII

The term of the corporation shall be perpetual.

### ARTICLE VIII

The corporation is organized on a membership basis. Each Member in good standing shall be entitled to one (1) vote on all issues on which Members are entitled to vote. The manner of exercise of such votes shall be determined in accordance with the corporation’s Bylaws. There will be no voting rights connected to any other investments.

Members shall remain in good standing by staying current in paying membership dues to Snowdrift.coop, Inc. and meeting any other membership requirements, as specified in the corporation’s Bylaws and Membership Agreement. The directors shall also be Members of the corporation. They shall serve as directors until their successors have been elected and qualified.

### ARTICLE IX

Any action required or permitted by the Act to be taken at an annual or special meeting of Members may be taken without a meeting, without prior notice, and without a vote, provided that:

Consent in writing, setting forth the action to be taken, is signed by a number of Members of the corporation not less than the minimum number of votes that would be necessary to authorize or take the action at a meeting at which all Members entitled to vote were present and voted.

Upon taking any corporate action in this manner, prompt notice of both the decision and the waived notice shall be given in writing to all Members, whether or not they participated in making the decision.

### ARTICLE X

No contract or other transaction between this corporation and any other corporation, firm, or association shall be subject to cancellation by the fact that any one or more of the directors or officers of the corporation are interested in or are directors or officers of the other corporation, firm, or association; and any director or officer may individually be a party to or be interested in any contract or transaction of the corporation. However, the contract or other transaction must be fair and reasonable to the corporation when it is authorized, approved, or ratified; and the material facts of the relationship or interest must be disclosed to the board or committee when it authorized, approved, or ratified the contract or transaction by a vote sufficient for the purpose without counting the vote of the interested director or officer. Every person who may become a director or an officer of the corporation is relieved from any liability that might otherwise exist from contracting with the corporation for the benefit of the officer or director or any firm, association, or corporation in which the officer or director may otherwise have an interest as set forth in these Articles.

### ARTICLE XI

Board of director members and volunteer officers, within the meaning of MCL 450.2556 et.seq. and pursuant to MCL Sec. 450.2209, shall not be liable to the corporation or its members for money damages for any action taken or any failure to take any action as a director or volunteer officer, except liability for any of the following:

i. The amount of a financial benefit received by a director or volunteer officer to which he or she is not entitled.
ii. Intentional infliction of harm on the corporation, its shareholders, or members.
iii. A violation of section 551.
iv. An intentional criminal act.
v. A liability imposed under section 497(a).

The corporation assumes the liability for all acts or omissions of a volunteer director, volunteer officer, or other volunteer occurring on or after the effective date of the provision that grants limited liability if all of the following are met:

i. The volunteer was acting or reasonably believed he or she was acting within the scope of his or her authority.
ii. The volunteer was acting in good faith.
iii. The volunteer’s conduct did not amount to gross negligence or willful and wanton misconduct.
iv. The volunteer’s conduct was not an intentional tort.
v. The volunteer’s conduct was not a tort arising out of the ownership, maintenance, or use of a motor vehicle for which tort liability may be imposed under section 3135 of the insurance code of 1956, 1956 PA 218, MCL 500.3135.

The liability of a volunteer director or volunteer officer for any act that occurred before the effective date of these Amended and Restated Articles of Incorporation shall only be subject to the requirements of this Article XI.

### ARTICLE XII

A quorum for holding a vote to amend these Articles is one third (1/3) of the entire Membership. An amendment passes if, at a meeting (or process) that met this quorum requirement, at least twice as many Members vote in favor of the amendment as against it. In no event shall any amendment make changes in the qualifications for Membership or the voting rights of Members without the consent of three quarters (3/4) of the Membership.

### ARTICLE XIII

If the existence of the corporation is terminated for any reason, all assets of the corporation remaining after the payment of obligations imposed by applicable law, including debts and expenses and return of loaned property, shall be distributed to a 501(c)(3) organization with a similar mission as may be specified in the Articles or Bylaws.

### ARTICLE XIV

When a compromise or arrangement or a plan of reorganization of this corporation is proposed between this corporation and its creditors or any class of them, a court of equity jurisdiction within the state, on application of this corporation or of a creditor or an application of a receiver appointed for the corporation, may order a meeting of the creditors or class of creditors to be affected by the proposed compromise or arrangement or reorganization, to be summoned in such manner as the court directs.

If a majority in number representing three-fourths (3/4) in value of the creditors or class of creditors to be affected by the proposed compromise or arrangement or a reorganization, agree to a compromise or arrangement or a reorganization of this corporation as a consequence of the compromise or arrangement, the compromise or arrangement and the reorganization, if sanctioned by the court to which the application has been made, shall be binding on all the creditors or class of creditors and also on this corporation.
