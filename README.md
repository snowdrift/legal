# [New Home on Codeberg](https://codeberg.org/snowdrift/governance)

This repository has been merged into the governance repo, which has been migrated to Codeberg: https://codeberg.org/snowdrift/governance

# Legal Issues

Note: alongside issues here and these pages, we have a
[legal discussion category at community.snowdrift.coop](https://community.snowdrift.coop/c/clear-the-path/legal)

## Formal policies

All these have been drafted but may not be in final form.

### Legal terms posted on the live site include:

* [Terms of Service](https://snowdrift.coop/terms)
    * [source file](https://gitlab.com/snowdrift/snowdrift/blob/master/website/templates/page/terms.hamlet)
* [Privacy Policy](https://snowdrift.coop/privacy)
    * [source file](https://gitlab.com/snowdrift/snowdrift/blob/master/website/templates/page/privacy.hamlet)
* [Trademark Policy](https://snowdrift.coop/trademarks)
    * [source file](https://gitlab.com/snowdrift/snowdrift/blob/master/website/templates/page/trademarks.hamlet)

### Organizational policies

* [Articles of Incorporation](articles.md) (filed in Michigan)
* [Bylaws](bylaws.md) (first complete draft, not final, not yet adopted)


### Internal policies we enforce, not strictly legal otherwise

* [Requirements for projects](https://wiki.snowdrift.coop/governance/project-reqs)
* [Code of Conduct](https://wiki.snowdrift.coop/community/conduct)

## Overall description

We have a separate page introducing the basic
[mechanism from a legal perspective](/legal-description).

## Discussion / research topics

### Non-profit cooperative goals

Our stated plan is to operate as a non-profit, multi-stakeholder cooperative
with three member classes:

* Workers: those paid to manage and develop the website
* Projects: those whose independent projects receive donations through the site
* Patrons: those who only donate

We are not stock-based but are funded by membership dues (which will be
calculated as pledges to the site just as for other projects we support).

### Federal 501 tax-exemption issues

Our public-goods mission fits a social purpose, not a profit or self-interested
purpose. Whether that fits the particular boxes and charitable tests of the IRS
is another question. See the [501 page](501.md) for more discussion on that.

### Ramifications of our mechanism for coordinating pledges

We have two possible implementations for handling funds. The first involves
holding funds and paying out to projects as they choose to withdraw them. The
second involves having a third-party system manage transactions that go directly
between patrons and projects. We describe the details of each on our
[**transactions page**](transactions.md). The first, fund-holding option is
technically cleaner and easier in all sorts of ways. But (as of early 2018 at
least) we're focusing on the legally-safer second option.

### Other legal concerns

* Accounting and reporting requirements
* Tax filing
    * 2013 was only private actions financially. 2014 was the first year of real
      activity for Snowdrift.coop accounts. All 2014 income was only non-taxable
      contributions.
        * Although we may need to consider some portion of crowdfunding rewards
          like shirts as being a taxable portion of the donations from the
          crowdfunding campaign, we still have spent basically 100% of income on
          expenses, i.e. no profit (and we don't need to do the 501(c)(3)
          special paperwork since we didn't offer tax-deductible claims to
          donors).
    * We need to determine clarity about what tax filings we need to do for
      projects that use the system or to what extent the filings are strictly
      the responsibility of each project.
* Anti-fraud measures
    * Online charities are a favorite target as one step in the process of
      credit card theft: a small charge is made with a stolen card number to
      confirm that it is still active. Botnets will often register many fake
      accounts with a site for this purpose. Many failed charge attempts from a
      single IP or many inactive accounts with failed charges are a sign of this
      kind of fraud. ([link with more](https://www.exratione.com/2010/10/three-necessary-defenses-for-open-credit-card-submission-forms/))
    * Normally the test charge amount is lost to the fraudster, but Gratipay
      was specifically targeted because it presented a mechanism by which the
      test amount could be recovered, through fake-account-to-fake-account
      tipping.
        * Snowdrift.coop has some built-in defenses against this, such as
          monthly transfers (takes longer to recover anything), externally
          variable share price (makes the amount recovered less certain),
          transparent donations (can't hide who's donating to whom), and our
          project vetting process (although the Gratipay scammers also made
          transfers to some legitimate bystander accounts to cover their
          tracks).
        * But we still need to be careful about credit card testing, since we
          allow users to withdraw sums from their accounts. The fraud prevention
          officers of whatever payment processors we end up using should be able
          to advise us on things like waiting period, maximum amount, etc.
    * We may not be able to tell users “your account is suspended pending
      investigation into potential fraud” because, if it has to be referred to
      law enforcement, we don't want the user notified that they are under
      investigation. Otherwise, [we could be held responsible](https://news.ycombinator.com/item?id=4744371)
      for “tipping off” the criminal and giving them a chance to escape. (This
      is part of the reason why PayPal has such a bad reputation.) It's unclear
      how to reconcile this with the goal of a transparent organization. Is it a
      solution to do all we can to avoid touching credit-cards ourselves and
      rely on the processing services?

### International legal issues

For funding projects outside the U.S. and for patrons outside the U.S., we're
not sure about the details, but this is a *complex* topic. Getting money from or
giving money to various countries has lots of issues.
