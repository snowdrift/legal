# Legal Request Policy

*Version 0.1*

Please serve requests to:

```
Snowdrift.coop
1021 Nottingham Rd.
Grosse Pointe Park, MI 48230
```

You may also send a courtesy copy to legal@snowdrift.coop.

Please make your requests as specific and narrow as possible, including the following information:

- Full information about authority issuing the request for information
- The name and badge/ID of the responsible agent
- An official email address and contact phone number
- The user, organization, repository name(s) of interest
- The URLs of any pages, gists or files of interest
- The description of the types of records you need

Please allow at least two weeks for us to be able to look into your request.
