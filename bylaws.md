# Snowdrift.coop old Bylaws draft (outdated)

The following is an outdated, never-adopted Bylaws draft.
We are working to redo the process and much has changed.

The working process is being tracked at:

- https://gitlab.com/snowdrift/governance/-/issues/76
- https://gitlab.com/snowdrift/governance/-/issues/49

## PREAMBLE: PRINCIPLES

*(Sources: [ICC bylaws](http://www.icc.coop/structure/policy/), [ica.coop](http://ica.coop/en/what-co-operative))*

### Section 1. Cooperative Values

As a cooperative organization, Snowdrift.coop values **self-help**, **self-responsibility**, **democracy**, **equality**, **equity**, and **solidarity**. Co-op members accept the ethics of **honesty**, **openness**, **social responsibility**, and **caring for others**.

### Section 2. Organizing Principles

We abide by the following principles of cooperation adapted from those established by the Rochdale Society of Equitable Pioneers:

* **Voluntary and Open Membership**: Membership in the co-op shall be voluntary and non-discriminatory. All who can use our services and accept the responsibilities of membership shall be eligible to join, regardless of race, nationality, political opinion, sex, gender, sexual orientation, age, or religious belief.
* **Democratic Participation**: Each member shall have one vote regardless of the amount of their investment; all members together control the organization.
* **Autonomy and Independence**: Co-ops are autonomous self-help organizations controlled by their members. If we enter into agreements with other organizations, including governments, or raise capital from external sources, we shall do so on terms that ensure democratic control by our members and maintain our cooperative autonomy.
* **Member Economic Participation**: Our members shall contribute to and democratically control our capital; that capital is the common property of the co-op. Members may allocate surpluses for any or all of the following purposes: developing the co-op; rebating members in proportion to their transactions with the co-op; and supporting other activities approved by the membership.
* **Education**: We shall provide ongoing education and training for members, elected representatives, managers, and employees so they can contribute effectively to the development of the co-op. We shall also inform the general public about the nature and benefits of cooperation.
* **Mutual Cooperation**: We shall actively cooperate on practical matters with other cooperatives at local, national, and international levels, to further serve their members and their communities.
* **Concern for Community**: We shall work for sustainable community development through policies approved by our members.

## CHAPTER 1: MISSION

*(Sources: [Articles of Incorporation](articles), [mission](mission))*

### Section 1: Vision

We envision a world where everyone has equal access to a robust and vibrant public commons; where everyone is empowered to realize and share their additions to our cultural heritage and to participate in the ongoing development of science and technology; and where there is liberty, privacy, and human dignity for all.

### Section 2: Mission

**Snowdrift.coop facilitates community support for projects that develop public goods.** Our online platform coordinates patrons in providing long-term funding for art, education, science, technology, and other sorts of non-rivalrous works under free/libre/open terms where the rights to access, use, modify, and distribute are *not* exclusively reserved. As a cooperative, we operate democratically so that our policies address the concerns of all stakeholders and serve the general public interest.

### Section 3: Transactions with Non-members

The co-op must offer all services openly to non-members. Co-op members shall not receive special benefits aside from their role in governance and acknowledgement.

## CHAPTER 2: MEMBERSHIP

*(Sources: <http://centralcoop.coop/index.php?page=bylaws>, [our co-op page](co-op), [ICC bylaws](http://www.icc.coop/structure/policy/), <http://www.rurdev.usda.gov/rbs/pub/cir1sec7.pdf>, <https://iocoop.org/policies/bylaws>)*

### Section 1: Eligibility

Any person agreeing with the purposes of the co-op, regardless of race, nationality, political opinion, sex, gender, sexual orientation, age, or religious belief, shall be eligible for membership upon becoming a patron of the Snowdrift.coop project (i.e., after two consecutive successful monthly donations as a patron of the Snowdrift.coop project within the Snowdrift.coop system).

To become a co-op member, an eligible patron must accept the terms of the membership agreement, as established by the Board of Directors. All members will be given a certificate of membership with information on their rights within the co-op in accordance with statutory requirement. No member may transfer their membership or any associated rights.

For the purpose of keeping the membership fees affordable to all, the Board may decide at any time to permit pledges to distinct sub-projects of the Snowdrift.coop project to count as valid patronage for co-op membership eligibility. Alternatively, the Board may allow pledges for reduced frequency such as bimonthly, quarterly, and so on.

### Section 2: Membership Classes

As a multistakeholder cooperative, Snowdrift.coop gives certain membership classes specific representation in governance. These classes are established solely by the nature of the member's participation in the business activities of the cooperative. Each co-op member will belong to one and only one class at any given time. Members shall be assigned to the most specific class for which they qualify and shall be automatically reassigned whenever their qualifications change.

For membership purposes, "project team member" is defined as "an official member of a project team, recipient of project funds, and/or participant in decision-making about the use of the funds, who is listed as such on the project's Snowdrift.coop page", i.e, employees of (or independent contractors employed by) the project.

* **Snowdrift class**: Snowdrift.coop project team members.
* **Project class**: Team members of other projects listed on Snowdrift.coop who are not on the Snowdrift.coop project team.
* **General class**: Members not belonging to any project team.

### Section 3: Membership Rights

The rights of a member in good standing shall include but not be limited to:

* The right to elect Directors to the Board and to recall any such elected Directors;
* The right to propose amendments to the Bylaws and the Articles of Incorporation;
* The right to submit a petition to the Board of Directors for a special meeting or non-meeting special election of the general membership;
* The right to participate in the leadership processes of the co-op through election to the Board of Directors and/or by serving on committees chartered by the Board of Directors;
* The right to obtain information concerning the actions of the Board of Directors, the operations of the co-op, the finances of the co-op, and the membership rolls of the co-op, except where such information is subject to privacy or confidentiality concerns.

### Section 4: Membership Responsibilities

Members in good standing are responsible for the following functions within the co-op:

* Providing the co-op with current and accurate contact information and keeping abreast of communications from the co-op,
* Holding the officers of Snowdrift.coop responsible for their actions,
* Participating in governance via elections, meetings, public comment periods, educational events, and other venues,
* Understanding the purpose, objectives, benefits, limitations, operations, finances, and long-term plans of the co-op,
* Applying cooperative values and principles to their actions and decisions within the co-op and in interactions with other organizations on behalf of the co-op, and
* Reporting any income derived from the co-op for legal purposes, as appropriate.

### Section 5: Suspension or Termination of Membership

If the funds in a member's Snowdrift.coop account become insufficient such that two consecutive monthly transfers to the Snowdrift.coop project fail to process, then the member shall be suspended and shall lose the rights afforded to members in good standing. Membership shall be reinstated upon adding funds and participating in a successful monthly transfer.

A member may voluntarily terminate their co-op membership at any time by dropping their pledge to the Snowdrift.coop project or otherwise notifying Snowdrift.coop of their intention to end their co-op membership. No fees shall be refunded upon member termination.

The Board of Directors shall provide procedures for disciplinary actions including the involuntary termination of any member violating any provisions of the Bylaws, Standing Rules, or membership agreement. Membership may only be terminated for cause. These procedures must ensure that the member is fully apprised of the allegations against them and has adequate opportunity to present evidence and offer testimony on their own behalf before the termination decision is finalized.

Membership terminates immediately, permanently, and automatically upon the death of the member.

## CHAPTER 3: MEETINGS

*(Sources: Consumer Cooperatives Statute, [ICC Bylaws](http://www.icc.coop/structure/policy/), <http://www.rurdev.usda.gov/rbs/pub/cir40/c40legdc.pdf>, <https://iocoop.org/policies/bylaws>)*

### Section 1: Annual Meeting

Snowdrift.coop shall hold a regularly occurring meeting of the general membership at least once annually. The meeting shall take place on February 1st with any physical gathering being at the primary place of business for the cooperative unless a different date and/or location is fixed by the Board.

The annual meeting shall include the installation of newly elected Board members and shall transact any other business properly brought before the general membership. The annual report shall be presented to the membership at the annual meeting.

At its discretion, the Board may also schedule other regularly occurring membership meetings.

### Section 2: Attendance

All general membership meetings shall make provisions so that members may attend by remote communication. Provided all members are able to observe and participate in all proceedings and roll is taken, remote communication shall be considered equivalent to physical attendance at the meeting.

### Section 3: Special Meetings and Elections

A special membership meeting or election may be called at any time by the Board of Directors, or by a written petition of members.

A proper petition must state the purpose or purposes for which the meeting or election is to be called. The Board must ensure conspicuous notice to the membership of any pending member petitions, and the petitions themselves must be readily accessible to read and sign. After a member petition has been signed by at least 10% of the total membership eligible to vote on the matter, the Board must arrange for a special meeting or election to take place within 21 to 90 days.

Any change from the precise wording of a member petition to that of the associated ballot question must be done with measures to enable the involvement and consent of the petitioners.

### Section 4: Notice of Meetings

At least 21 days before any membership meeting, the Board shall provide to all members a written notice of meeting time, agenda, and place and/or communication medium. The notice must include instructions on how to attend by remote communication. The Board of Directors shall provide notice by, at minimum, displaying a banner on the public website and messaging site accounts and email addresses on file for all members. At its discretion, the Board may also publicize meeting information through other channels, especially in response to member request.

### Section 5: Voting

Every Snowdrift.coop member is entitled to a single vote on any office to be filled or issue under consideration for which their member-class is entitled to vote.

All elections shall be conducted via a score voting system (see <http://rangevoting.org> for more information) where voters assign each candidate a score from 1 to 10 with an option to register "no opinion". The winner (or winners, if the election is for more than one interchangeable seat) shall be determined by highest average score(s). For positions elected by all member-classes, the scores shall first be averaged by class, then the class averages shall be averaged together to determine the final score for each candidate.

For ballot questions, the opinion range shall be a 6-point approval scale with ratings 1-6 labeled as "strongly disapprove, disapprove, somewhat disapprove, somewhat approve, approve, strongly approve." Passage of a measure will require each and every relevant member class to have an average score of at least 3 and the average of the class averages to be at least 4.

Quorum for an election shall be 10% of the total eligible voters for each and every member class. A meeting without quorum may adjourn to a future time by a majority vote of those present. Any action that may be taken at any regular or special meeting, including election of Directors, may also be taken outside of meetings through a ballot in which the same quorum and approval requirements shall apply. Proxy voting is not permitted but any member eligible to participate in an election may call the vote to written or electronic secret ballot distributed to all eligible members. Electronic transmission of ballots through a Board-approved portal is permitted but all voting systems electronic or otherwise must be auditable.

Whenever a membership-elected position is open due to vacancy or expiration of term, any member qualified to fill that position is eligible to submit a nomination petition, and will appear on the ballot if the petition is signed by 5% of the members eligible to vote for that position. The Board may establish procedures whereby it may select up to 3 additional candidates per position to appear on the ballot, provided that the nominees consent to appear.

## CHAPTER 4: THE BOARD OF DIRECTORS

*(Sources: <http://www.oklahomafood.coop/Display.aspx?cn=board>, <http://centralcoop.coop/index.php?page=bylaws#board>, <https://iocoop.org/policies/bylaws>, <http://www.citizens.coop/aboutus/bylaws.shtm>)*

### Section 1: Composition

The Board of Directors shall be made up of Representatives elected by the co-op membership.

Each member class shall be entitled to one Representative Director elected from and by that class' members, for a total of three Member-Class Representatives.

All member classes shall together elect all At-Large Representatives. There shall be at least three At-Large Representatives at any time. At the discretion of the Board, additional At-Large Representative seats may be established in accordance with the growth of the general membership, up to a total of six At-Large Representatives.

The total number of Representatives shall, therefore, be between six and nine.

### Section 2: Term Length and Limits

Representatives shall serve three-year terms, beginning at the annual membership meeting. Elections shall rotate on a three-year cycle such that each year seats for one or two At-Large Representatives and one Member-Class Representative will be up for election. Those At-Large seats up for election each year are filled by the overall top scoring At-Large candidates.

No Representative may serve more than two consecutive terms in the same role, except in the event that no other fully qualified candidate is nominated to run for that role in the upcoming election.

### Section 3: Qualifications

All Directors must be co-op members in good standing aged 16 or above. Members under age 16 are ineligible to serve on the Board but may serve on Board-chartered committees.

Loss of required qualifications (as in the case of suspension or termination of membership) shall result in immediate termination of a Representative's position. Whenever a Representative's membership class changes, they must resign their position until the next regular Board election.

### Section 4: Vacancies

Whenever a vacancy occurs among the Representatives, other than by expiration of a term of office, the Board shall appoint a fully qualified member to fill the vacancy until the next regular membership meeting. In addition to any other elections scheduled for that meeting, a special election shall be held to fill any remaining time in the vacated Representative's term.

One or more Representatives may be recalled by a ballot measure approved by their constituent classes, with or without cause.

Any Representative may resign via written notice to the Board, effective immediately unless another time is specified in the letter of resignation.

### Section 6: Meetings

The Board of Directors shall hold at least one regularly scheduled meeting per calendar month. Special meetings of the Board may be called by or at the request of any Representative. Reasonable notice shall be given of time, date, agenda, and location/communication medium for all Board meetings, as defined in the Standing Rules.

Attendance at meetings by remote communication shall be considered equivalent to physical attendance, provided that all attendees can effectively participate in all proceedings and roll is taken. A quorum for the purpose of conducting Board business shall require a majority of the Representatives then in office plus an additional person to serve as facilitator. If quorum is not available, a majority vote of those present may adjourn the meeting to a future time with the standard notification procedures for any meeting. The Board of Directors is empowered to specify remedies and penalties for absentee Representives in the Standing Rules.

### Section 7: Consensus Decision-making

The default decision-making process for the Board shall be facilitated consensus with focus on developing collaborative solutions to open-ended questions about improving co-op operations and governance. Any number of calls for consensus may be made and discussion may continue until agreements are reached.

When one or more Representatives are absent from a meeting, any consensus decision by those present may be finalized by the assent of all absent Directors via a public, verifiable, online forum. If any absent Representatives register dissent or a request to wait, their concerns must be accounted for and their assent given before the measure is passed. If any absent Representatives fail to register an opinion within 21 days, then any quorum present at the next Board meeting may finalize the original decision whether or not those absent Representatives are now present.

If an issue is an emergency, where "emergency" is understood as "substantial matter that must be acted upon within the next 21 days", then a decision may be finalized in a single meeting without the assent of any absent Representatives, although this is not preferred. Single meeting approval is prohibited for changes to the Membership Agreement or to any other policy that contains a public comment period clause (e.g., the website Terms of Use or Privacy Policy), the proposed annual budget, or proposed capital expenditures. Such issues require a comment period of at least 21 days between proposal and finalization. A comment period is also strongly recommended for any other issues that impact member or user rights or site functionality, or for major financial decisions.

### Section 8: Fallback Voting

In cases of irreconcilable deadlock or on an emergency issue, the facilitator may call for a "fallback" vote on a 6-point approval scale with an average score of at least 4 required to pass. All ballots should be collected before tabulation but do not remain secret.

Any Representative may block a fallback vote on substantive — but not on personal — grounds. "Substantive grounds" here indicates adherence to legal principles or to organizational integrity (particularly as expressed in the Articles and Bylaws), such as a reasonable concern that a proposed solution violates the cooperative principles or the co-op mission.

If a block has been raised, the blocking Representative must form a reconciliation committee with all other interested parties and attend at least one meeting. If the blocking Representative does not attend, the block fails, the original proposed solution returns to the Board for consideration, and that Representative may not block the vote again.

If the reconciliation committee reaches consensus, their proposal returns to the Board for consideration. New amendments may then be proposed but can be rejected by any member of the reconciliation committee. The proposal may then be called to a fallback vote which cannot be blocked.

If the reconciliation committee does not reach consensus within a specified time frame, the proposal is provisionally blocked. For the block to become final, the blocking Representative must post a public written explanation of their reason for blocking within 3 days of the final committee meeting. If the written explanation is not produced, the original proposed solution returns to the Board for consideration and may not be blocked again.

### Section 9: Responsibilities

The Board shall be in charge of the general operations of the co-op. In accordance with the values, principles, and purposes laid out in the Articles of Incorporation and in these Bylaws, it shall receive and compile comments and concerns from members, stakeholders, and outside parties, and work to address such concerns. Each Board member is responsible for reading and understanding the organizational documents and ensuring the Board's compliance with them. The Board is charged with conducting regular review of all major policies (including the Standing Rules, Bylaws, and site policies) to ensure that all are complete and up-to-date.

Standing or temporary committees may be chartered by the Board from time to time and delegated powers fitting each committee's defined purpose, subject to conditions prescribed by the Board and by law. Committee discussions shall be held in a public forum unless privacy or confidentiality concerns prevent this. All committees shall record the minutes of their meetings and forward those minutes to the Secretary for inclusion in the official records of the corporation. The Board retains final responsibility for duties and powers delegated to any committees.

The Board shall hire or appoint a General Manager who shall be responsible for day-to-day operations of the co-op and have responsibility for managing and hiring employees and independent contractors, subject to any provisions specified in the co-op's Standing Rules. The Board is empowered to obtain the services of an external ombudsperson to assist members with grievances in navigating co-op governance.

The Board shall install an adequate accounting system to meet the requirements of the business and ensure proper record-keeping for all business transactions. The Board shall select one or more banks to act as depositories of the co-op's funds and determine the manner of receiving, depositing, and disbursing funds; the check form; and the person or persons who will sign checks; with the power to change banks, checks, and signatories at their discretion.

Representatives may be required to complete any training deemed necessary to carry out their duties, so long as it does not pose an unreasonable barrier to participation. The Board shall conduct a self-assessment of its performance on at least an annual basis and adjust training requirements for the coming year accordingly.

All other powers and authorities of the co-op may be exercised by the Board, subject to the laws of the State of Michigan, to the Articles of Incorporation, and to the Bylaws.

### Section 10: Conflicts of Interest

Representatives are under an affirmative obligation to disclose any actual, potential, or perceived conflicts of interest. When such a disclosure is made or a conflict of interest is otherwise discovered, the remaining Representatives shall determine what action, if any, to take. At minimum, Representatives with conflicts of interest may not participate in discussion or decision-making in matters related to the conflict. Representatives with conflicts of interest may also be asked to take steps to compartmentalize the conflicting interests or, in extreme cases, to tender resignation.

### Section 11: Compensation

Representatives may, at the discretion of the Board, seek reimbursement from the co-op for regular and necessary expenses incurred in the execution of their duties.

The Board may, at its discretion, propose a compensation scheme for Representatives which will take effect only after approval by the general membership at an annual or special general meeting. The text of the proposal submitted for the election must have prior approval from a six person committee chosen from the general membership where no member expects to receive any compensation from the approved scheme.

## CHAPTER 5: OFFICERS

The Offices of President, Vice President(s), Treasurer, and Recording Secretary shall be appointed by the Board and shall serve an unlimited number of consecutive one-year terms beginning at the annual membership meeting. The Board may remove an appointed Officer at any time.

The positions of President, Treasurer, and Recording Secretary must be aged 18 or older.

The **President** shall be the chief executive officer of the corporation; shall set the agenda and be the default facilitator for all meetings of the membership and meetings of the Board of Directors; shall execute all documents and reports required by law and as directed by the Board; shall have custody of money together with the Treasurer and the General Manager; and shall report at each Annual Meeting of the corporation.

The **Treasurer** shall be the business and finance officer of the corporation and shall report regularly to the Board on the financial condition of the co-op; shall execute all documents and reports required by law; and shall contract for an annual independent financial audit and report to the Board. The Treasurer shall also be responsible for monitoring and evaluating the financial management of Snowdrift.coop by the staff. The Treasurer shall be authorized to serve as chair of any and all committees dealing with corporate finances.

The **Recording Secretary** shall attend all Annual and Special Meetings of the membership and all meetings of the Board; shall record the minutes of all such proceedings; shall receive and archive minutes and other records from all Board-chartered committees; shall make such records accessible to the co-op membership on the co-op website (with any necessary redactions for privacy or other legal concerns); shall serve all notices and sign all reports and documents as directed by the Board; and shall have custody of the corporate seal and affix the seal to all documents as directed by the President or the Board.

The **Vice President(s)** shall chair any committees assigned to them by the Board and shall be responsible for the efficient functioning of these committees. Each VP shall arrange for their designated committees to report to the Board at least every two months. In the absence of the President, or in the event of their inability or refusal to act, the Vice Presidents shall take on the duties, have the powers, and be subject to the restrictions of the office of President. The Vice Presidents shall also perform such other duties as from time to time may be assigned to them by the Board.

The offices of Treasurer and Recording Secretary may be held concurrently by the same individual. The Board may exercise discretion to further consolidate Officer positions where the consolidation does not impede the functioning of the cooperative, present a conflict of interest, or place an undue burden on the individual filling the consolidated office.

## CHAPTER 6: EMPLOYEES

*(Sources: <http://www.our.coop/content/co-op-bylaws>, <http://www.uky.edu/Ag/AgriculturalEconomics/pubs/ext_ca/aec60.pdf>)*

The General Manager is empowered to hire, supervise, and dismiss employees as they deem beneficial for the operations of the co-op. The Board shall establish policies in the Standing Rules for employment, to include (but not limited to) description of duties, hiring, evaluation, compensation, non-discrimination, and dismissal.

The General Manager is prohibited from serving in any Board capacity but may vote in elections as a Snowdrift-class member.

## CHAPTER 7: SURPLUS ALLOCATIONS

*(Sources: <https://iocoop.org/policies/bylaws>)*

“Surplus” is defined as the excess of revenues and gains over expenses and losses for a fiscal year. The Board may exercise its discretion to apportion the surplus for rectifying a deficit in retained earnings, increasing operating reserves, funding planned capital improvement, contributing to upstream projects, and/or reducing future membership fees through new options for or adjustments to ongoing patronage pledges.

Under no circumstances may a deficit in revenues over expenses be remedied by a levy on member accounts.

## CHAPTER 8: AMENDMENT AND REORGANIZATION

*(Sources: incorporating statute, [Articles of Incorporation](articles))*

*(note: the Articles need to be amended to consider member-class issues and statute concerns and our new amendment procedures.)*

As per the procedure specified in the Articles of Incorporation, until the first general member meeting, amendments to the Bylaws not affecting member voting rights may be approved by a consensus of the Board after a 21-day public comment period. At the first member meeting, the Bylaws as they stand on that date will be presented to the membership for ratification.

After the first member meeting, proposed changes to the Bylaws or to the Articles of Incorporation may only be approved by a general membership 6-point approval vote where the majority of all voters in each and every member class are supportive of the measure, each and every class scores an average of at least 3.5, and the average of the class scores is at least 4. As per Sections 1145, 1146, and 1161 of Chapter 450 of the Michigan Compiled Laws, such amendments may also be subject to a confirmation vote if initially approved by less than 50% of the total membership. This procedure shall also be required for any proposal to merge with, acquire, or be acquired by another legal entity, or to dissolve the co-op.

In the event of the liquidation, dissolution, or winding up of the affairs of the co-op, whether voluntary or involuntary, after paying or providing for the payment of all debts as provided by law, each member will be entitled to receive an amount in cash equal to the remaining sum in their Snowdrift.coop accounts. All of the remaining balance of the assets of the co-op will be distributed among nonprofits supporting grassroots cooperative organizing on an equitable basis, as determined by the Board.

## CHAPTER 9: DISCLAIMER OF FIDUCIARY STATUS

(*Source: [Terms of Use](terms-of-use)*)

While the Board of Directors has a fiduciary duty to the members of the co-op, Snowdrift.coop as a corporation does not act as a trustee or a fiduciary of site users; our services are provided to users administratively. We act only as an intermediary in coordinating donations from patrons to specific projects that serve our mission. We do not provide any banking or money transfer services. Snowdrift.coop is not a “financial institution” as defined under the Bank Secrecy Act (BSA). Any payments transferred through Snowdrift.coop are not insured against default, loss, or forfeiture.

Snowdrift.coop is intended purely as a conduit between patrons and projects. Our systems are not to be used for the sale of goods and services, for holding funds intended for any purpose besides patronizing listed projects, or for any other type of financial transaction beyond monthly patronage transfers and payouts to project team members.

## CHAPTER 10: INDEMNITY AND BONDING

The cooperative shall indemnify and hold harmless each person who serves as a Director or Officer from and against any and all claims and liabilities for action alleged to have been taken or omitted by them in their organizational capacity, and for all legal and other expenses reasonably incurred in connection with any such claim or liability. However, no person shall be indemnified against or be reimbursed for any expense in connection with any claim or liability arising out of their own negligence, willful misconduct, or bad faith. Rights listed here shall not exclude anyone from other rights to which they may be lawfully entitled. The co-op further retains all rights to indemnify or reimburse individuals in any other proper cases.

The Board shall require the Treasurer and any other Officer, agent, or employee of the co-op charged with custody of any of its funds or property to give bond in whatever sum and with whatever surety as the Board shall in its discretion deem fit. The Board may also require any other Officer, agent or employee of the co-op to give bond in whatever amount and with whatever surety as it shall determine. The costs of all such bonds shall be borne by the co-op.

The co-op, its Directors, Officers, employees, and agents shall be fully protected in taking any action or making any payment necessary under this Indemnity Chapter of the Bylaws, or refusing to do so, in reliance on advice from counsel.

## CHAPTER 11: SEVERABILITY

A determination that any provision of these Bylaws is for any reason inapplicable, invalid, illegal, or otherwise ineffective will not affect or invalidate any other provision of these Bylaws.
